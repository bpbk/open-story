<?php
require_once( get_template_directory() . '/libs/custom-ajax-auth.php' );

add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup()
{
	load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
	add_theme_support( 'title-tag' );
	add_theme_support( 'automatic-feed-links' );
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'small', 200 );
	add_image_size( 'productthumb', 500 );
	add_image_size( 'medium', 800 );
	add_image_size( 'large', 1200 );
	add_image_size( 'full', 1900 );
	global $content_width;
	if ( ! isset( $content_width ) ) $content_width = 640;
	register_nav_menus(
	array( 'main-menu' => __( 'Main Menu', 'blankslate' ) )
	);
}

add_filter('show_admin_bar', '__return_false');

add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts()
{
	wp_enqueue_script( 'jquery' );

	wp_register_script( 'scripts', get_template_directory_uri()."/js/scripts.js");
	wp_enqueue_script("scripts");

	wp_register_script( 'cordova', get_template_directory_uri()."cordova.js");
	wp_enqueue_script("cordova");

	wp_register_script( 'framework7js', get_template_directory_uri()."/lib/framework7/js/framework7.min.js");
	wp_enqueue_script("framework7js");

	wp_enqueue_style( 'framework7', get_template_directory_uri().'/lib/framework7/css/framework7.ios.min.css' );

	wp_enqueue_style( 'framework7colors', get_template_directory_uri().'/lib/framework7/css/framework7.ios.colors.min.css' );
}
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script()
{
if ( get_option( 'thread_comments' ) ) { wp_enqueue_script( 'comment-reply' ); }
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '&rarr;';
} else {
return $title;
}
}
add_filter( 'wp_title', 'blankslate_filter_wp_title' );
function blankslate_filter_wp_title( $title )
{
return $title . esc_attr( get_bloginfo( 'name' ) );
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init()
{
register_sidebar( array (
'name' => __( 'Sidebar Widget Area', 'blankslate' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => "</li>",
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
function blankslate_custom_pings( $comment )
{
$GLOBALS['comment'] = $comment;
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'blankslate_comments_number' );
function blankslate_comments_number( $count )
{
if ( !is_admin() ) {
global $id;
$comments_by_type = &separate_comments( get_comments( 'status=approve&post_id=' . $id ) );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}

function pluginname_ajaxurl() { ?>
	<script type="text/javascript">
		var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
		var templateurl = '<?php echo get_template_directory_uri(); ?>';
		var homeurl = '<?php echo get_home_url(); ?>';
		var siteurl = '<?php echo site_url(); ?>';
	</script>
<?php
}


add_action('wp_ajax_nopriv_do_ajax', 'ajax_function');
add_action('wp_ajax_do_ajax', 'ajax_function');

function ajax_function(){
     switch($_REQUEST['fn']){
        case 'create_story':
        	$output = create_story_post($_REQUEST['sTitle'], $_REQUEST['cTitle'], $_REQUEST['cText'], 19, array(array(1.23043, 0.2342114)));
            echo $output;
        break;
        default:
            $output = 'No function specified, check your jQuery.ajax() call';
        break;
     }
     die;
}

function create_story_post($request){
	$request = json_decode($request['story']);
	$sTitle = $request->stitle;
	$sGenres = json_decode($request->genres);
	$sLocations = json_decode($request->slocs);
	$cTitle = $request->ctitle;
	$cText = $request->ctext;
	$sAuth = $request->user;
	$storyArgs = array('post_author' => $sAuth, 'post_title' => $sTitle, 'post_type' => 'story', 'post_status' => 'publish');
	$storyID = wp_insert_post($storyArgs);
	wp_set_object_terms($storyID, $sGenres, 'genre');
	if(isset($storyID) && $storyID != 0){
		return create_location_connections($storyID, $sAuth, $cTitle, $cText, $sGenres, $sLocations);
	}else{
		return 0; // something went wrong.
	}
}

function create_location_connections($storyID, $sAuth, $cTitle, $cText, $sGenres, $sLocations){
	$allLocs = array();
	foreach($sLocations as $sLoc){
		$locArgs = array('post_author' => $sAuth, 'post_title' => $sLoc[0].', '.$sLoc[1], 'post_type' => 'story_location', 'post_status' => 'publish');
		$locID = wp_insert_post($locArgs);
		wp_set_object_terms($locID, $sGenres, 'genre');
		if(isset($locID) && $locID != 0){
			$field_key = "story_map";
			$value = array( 'lat' => $sLoc[0], 'lng' => $sLoc[1] );
			update_field( $field_key, $value, $locID );
			update_field('story_lat', $sLoc[0], $locID);
			update_field('story_lng', $sLoc[1], $locID);
			array_push($allLocs, $locID);
		}
	}
	if(count($allLocs > 0)){
		//connect with ACF
		//$field = get_field_object('stories_x_locations');
		update_field('stories_x_locations', $allLocs, $storyID);
		return create_chapter($storyID, $sAuth, $cTitle, $cText, $sGenres, $allLocs);
	}else{
		return 0;
	}
}

function create_chapter($storyID, $sAuth, $cTitle, $cText, $sGenres, $locationIDs){
	$chapterArgs = array('post_author' => $sAuth, 'post_title' => $cTitle, 'post_status' => 'publish', 'post_content' => $cText, 'post_type' => 'chapter', 'menu_order' => 1);
	$chapterID = wp_insert_post($chapterArgs);
	wp_set_object_terms($chapterID, $sGenres, 'genre');
	if(isset($chapterID) && $chapterID != 0){
		// ACF Connect locations to chapters
		//$locsxchaps = get_field_object('locations_x_chapters');
		update_field('locations_x_chapters', $locationIDs, $chapterID);

		// ACF Connect story to chapter
		//$storiesxchapters = get_field_object('stories_x_chapters');
		update_field('stories_x_chapters', $chapterID, $storyID);

		return 1;
	}else{
		return 0;
	}
}

function get_story_locations($sID)
{
	$connected = new WP_Query( [
		'post_type' => 'story_location',
		'meta_query' => [
			[
				'key' => 'stories_x_locations',
				'compare' => 'LIKE',
				'value' => $sID
			]
		],
	] );
	if ( $connected->have_posts() ) :
		$locs = array();
		while ( $connected->have_posts() ) : $connected->the_post();
	    	$loc = array('lat'=>get_field('story_lat'), 'lng'=>get_field('story_lng'), 'id'=>get_the_id());
	    	array_push($locs, $loc);
		endwhile;
		wp_reset_postdata();
		return $locs;
	endif;
}

function get_user_stories() // should make this paged at some point.
{
	global $current_user;
	// Get all chapters the user has written
	$args = array('author' => $_REQUEST['user'], 'post_type' => 'chapter', 'posts_per_page' => -1);
	$chapter_query = new WP_Query( $args );
	if ( $chapter_query->have_posts() ) :
		$stories = (object)array();
		$iWrote = array();
		$iContributed = array();
		while ( $chapter_query->have_posts() ) : $chapter_query->the_post();
			$chapId = get_the_id();
			if($connectedStories = get_field('stories_x_chapters')) {
				foreach($connectedStories as $cStory) {
					$storyObj = (object)array();
					$storyObj->title = get_the_title($cStory);
					$storyObj->id = $cStory;
					$genres = get_story_genres($cStory);
					$storyImage = get_the_post_thumbnail_url($cStory, 'medium');
					if($storyImage == null){
						$storyImage = $genres[0]->image;
					}
					$locs = get_story_locations($cStory);
					$storyObj->genres = $genres;
					$storyObj->locs = $locs;
					$storyObj->type = 'wrote';
					$storyObj->image = $storyImage;
					array_push($iWrote, $storyObj);
				}
			} else if ($chapterLocations = get_field('locations_x_chapters')) { // if no stories are associated, get the location that's associated because this is a post they don't own but did contribute to.
				foreach($chapterLocations as $cLoc) {
					$locs = [
						[
							'lat'=>get_field('story_lat', $cLoc),
							'lng'=>get_field('story_lng', $cLoc),
							'id'=>$cLoc
						]
					];
					if($locationStories = get_field('stories_x_locations', $cLoc)){
						foreach($locationStories as $lStory) {
							$cStoryId = $lStory;
							$authorId = get_post_field( 'post_author', $lStory );
							$storyObj = (object)array();
							$storyObj->title = get_the_title($lStory);
							$storyObj->id = $cStoryId;
							$genres = get_story_genres($cStoryId);
							$storyImage = get_the_post_thumbnail_url($lStory, 'medium');
							if($storyImage == null){
								$storyImage = $genres[0]->image;
							}
							$storyObj->author = get_story_author($authorId);
							$storyObj->genres = $genres;
							$storyObj->locs = $locs;
							$storyObj->type = 'contributed';
							$storyObj->image = $storyImage;
							array_push($iContributed, $storyObj);
						}
					}
				}
			}
		endwhile;
		$stories->iwrote = $iWrote;
		$stories->icontributed = $iContributed;
		wp_reset_postdata();
	endif;

	// Get users daily stories
	$dailyArgs = array('author' => $_REQUEST['user'], 'post_type' => 'daily', 'posts_per_page' => -1);
	$daily_query = new WP_Query( $dailyArgs );
	if ( $daily_query->have_posts() ) :
		$dailyStories = array();
		while ( $daily_query->have_posts() ) : $daily_query->the_post();
			$storyObj = (object)array();
			$storyId = get_the_id();
			$storyObj->title = get_the_title();
			$storyObj->id = $storyId;
			$genres = get_story_genres($storyId);
			$storyImage = wp_get_attachment_image_url(get_post_thumbnail_id(), 'medium');
			if($storyImage == null){
				$storyImage = $genres[0]->image;
			}
			$storyObj->genres = $genres;
			$storyObj->type = 'daily';
			$storyObj->image = $storyImage;
			$storyObj->dailytext = get_the_content();
			if($dStories = get_field('prompts_x_dailies')) {
				foreach($dStories as $dStory) {
					$prompt = get_the_title($dStory);
					$date = get_the_date('F j, Y', $dStory);
					$storyObj->prompt = array('prompt_text'=>$prompt, 'date' => $date);
				}
			}
			array_push($dailyStories, $storyObj);
		endwhile;
		$stories->daily = $dailyStories;
		wp_reset_postdata();
	endif;
	echo json_encode($stories);

	die();
}

function get_story_author($aid){
	$authorMeta = get_user_meta($aid);
	$theAuthor = (object)array();
	$theAuthor->id = $aid;
	$authorImage = get_field('user_photo', 'user_'.$aid);
	$theAuthor->username = $authorMeta['nickname'][0];
	$theAuthor->firstname = $authorMeta['first_name'][0];
	$theAuthor->lastname = $authorMeta['last_name'][0];
	$theAuthor->description = $authorMeta['description'][0];
	$theAuthor->image = $authorImage['sizes']['small'];
	return $theAuthor;
}

function get_story_genres($sid){
	$genres = get_the_terms($sid, 'genre');
	$genreArray = array();
	foreach($genres as $genre){
		$genreImage = get_field('genre_image', $genre);
		$genreImage = $genreImage['sizes']['medium'];
		$genreObj = (object)array();
		$genreObj->term_id = $genre->term_id;
		$genreObj->slug = $genre->slug;
		$genreObj->name = $genre->name;
		$genreObj->image = $genreImage;
		array_push($genreArray, $genreObj);
	}
	return $genreArray;
}

function get_story_chapters($data){
	$chapters = new WP_Query( array(
		'posts_per_page' => -1,
		'post_type' => 'chapter',
		'meta_query' => [
			[
				'key' => 'locations_x_chapters',
				'compare' => 'LIKE',
				'value' => $data['id']
			]
		],
	  'post_status' => array('private', 'publish', 'public'),
	  'orderby' => 'date',
	  'order' => 'ASC',
	) );
	// Display connected pages
	if ( $chapters->have_posts() ) :
		$chaptersArray = array();
		while ( $chapters->have_posts() ) : $chapters->the_post();
	    	$chapterObj = (object)array();
	    	$chapterObj->title = get_the_title();
	    	$chapterObj->id = get_the_id();
	    	$chapterObj->content = get_the_content();
	    	$chapterObj->author = get_the_author();
	    	$chapterObj->authorid = get_the_author_id();
	    	array_push($chaptersArray, $chapterObj);
		endwhile;
		wp_reset_postdata();
		echo json_encode($chaptersArray);
	endif;
	die();
}


function get_all_genres(WP_REST_Request $request){
	$terms = get_terms(array('hide_empty' => false, 'taxonomy' => 'genre'));
	echo json_encode($terms);
	die();
}

function get_my_user_info(WP_REST_Request $request){
	$userInfo = get_user_meta($request['id']);
	$userObj = (object)array();
	$userObj->username = $userInfo['nickname'][0];
	$userObj->firstname = $userInfo['first_name'][0];
	$userObj->lastname = $userInfo['last_name'][0];
	$userObj->desc = $userInfo['description'][0];
	$myGenres = get_field('my_genres', 'user_'.$request['id']);
	if(isset($myGenres)){
		$userObj->genres = $myGenres;
	}
	echo json_encode($userObj);
	die();
}

function add_chapter(WP_REST_Request $request){
	$chapterArgs = array('post_author' => $request['auth'], 'post_title' => $request['ctitle'], 'post_content' => $request['ctext'], 'post_type' => 'chapter', 'menu_order' => $request['order'], 'post_status' => 'publish');
	$chapterID = wp_insert_post($chapterArgs);
	if(isset($chapterID) && $chapterID != 0){
		wp_set_object_terms($chapterID, json_decode($request['genres']), 'genre');
		// ACF connect location to chapter
		//$locsxchapters = get_field_object('locations_x_chapters');
		update_field('locations_x_chapters', $request['id'], $chapterID);

		return 1;
		die();
	}
}

add_action( 'rest_api_init', function () {

	register_rest_route( 'openstory/v2', '/regloguser/', array(
		'methods' => 'GET',
		'callback' => 'reglog_user',

	));
	register_rest_route( 'openstory/v2', '/submitusergenres/', array(
		'methods' => 'GET',
		'callback' => 'submit_user_genres',

	));
	register_rest_route( 'openstory/v2', '/saveuserimage/', array(
		'methods' => 'POST',
		'callback' => 'save_user_image',

	));
	register_rest_route( 'openstory/v2', '/getchapters/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'get_story_chapters',
		'args' => array(
			'id' => array(
				'validate_callback' => function($param, $request, $key) {
					return is_numeric( $param );
				}
			),
		),
	));
	register_rest_route( 'openstory/v2', '/submitstory/', array(
		'methods' => 'GET',
		'callback' => 'create_story_post',
	));
	register_rest_route( 'openstory/v2', '/getgenres/', array(
		'methods' => 'GET',
		'callback' => 'get_all_genres',

	));
	register_rest_route( 'openstory/v2', '/getmyuserinfo/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'get_my_user_info',
	));

	register_rest_route( 'openstory/v2', '/getnearbylocations/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'get_nearby_locations',
	));

	register_rest_route( 'openstory/v2', '/addchapter/(?P<id>\d+)', array(
		'methods' => 'GET',
		'callback' => 'add_chapter',
	));

	register_rest_route( 'openstory/v2', '/gettweets/', array(
		'methods' => 'GET',
		'callback' => 'get_tweets_daily',
	));

	register_rest_route( 'openstory/v2', '/getdaily/(?P<user>\d+)', array(
		'methods' => 'GET',
		'callback' => 'get_daily',
	));

	register_rest_route( 'openstory/v2', '/gettodaysstories/(?P<prompt>\d+)', array(
		'methods' => 'GET',
		'callback' => 'get_todays_stories',
	));

/*
	register_rest_route( 'openstory/v2', '/getdailystory/', array(
		'methods' => 'GET',
		'callback' => 'get_daily_story',
	));
*/

} );

function no_stories()
{
	echo -1;
}

add_action("wp_ajax_user_stories", "get_user_stories");
add_action("wp_ajax_nopriv_user_stories", "get_user_stories");

function already_logged_in()
{
	global $current_user;
	echo json_encode($current_user);
	die();
}

function login()
{
	$creds = array();
	$creds['user_login'] = $_GET["username"];
	$creds['user_password'] = $_GET["password"];
	$creds['remember'] = true;

	$user = wp_signon($creds, false);
	if (is_wp_error($user))
	{
		echo "FALSE";
		die();
	}

	wp_set_current_user( $user->ID, $creds['user_login'] );
	wp_set_auth_cookie( $user->ID, true );
	do_action( 'wp_login', $creds['user_login'] );

	if ( !is_user_logged_in() ) :
		echo 'FALSE';
		die();
	endif;
	echo json_encode($user);
	die();
}

add_action("wp_ajax_login", "already_logged_in");
add_action("wp_ajax_nopriv_login", "login");

function reglog_user(WP_REST_Request $request){
	if($request['reglog'] == 'register'){ // user is attempting to register
		$email = urldecode($request['email']);
		$password = urldecode($request['password']);
		$username = urldecode($request['username']);
		if(username_exists($username) == false && email_exists($email) == false) {
			$userID = wp_create_user($username, $password, $email);
			$user = new WP_User( $userID );
			$user->set_role('contributor');
			return $userID;
	   	}else{
		   	$registerError;
		   	if(username_exists($username)){
			   	$registerError = 'username';
		   	}
		   	if(email_exists($email)){
			   	$registerError = 'email';
		   	}
		   	return $registerError;
	   	}
   	}else{ // user is logging in.
	   	$creds = array();
	   	$emailuser = $_GET["email"];
		$creds['user_login'] = urldecode($emailuser);
		$creds['user_password'] = $_GET["password"];
		$creds['remember'] = true;
		if (filter_var($creds['user_login'], FILTER_VALIDATE_EMAIL)) { //Invalid Email
			$userlogin = get_user_by('email', $creds['user_login']);
	        $creds['user_login'] = $userlogin->data->user_login;
	    }
		$user = wp_signon($creds, false);
		if (is_wp_error($user))
		{
			$error_string = $user->get_error_message();
			echo $error_string;
			die();
		}

		wp_set_current_user( $user->ID, $creds['user_login'] );
		wp_set_auth_cookie( $user->ID, true );
		do_action( 'wp_login', $creds['user_login'] );

		if ( !is_user_logged_in() ) :
			echo 'FALSE';
			die();
		endif;
		echo json_encode($user);
		die();
   	}
/*
	if(username_exists('brandon2') == false && email_exists('brandon@fullmetalworkshop.com') == false) {
		$userID = wp_create_user('brandon2', 'lo9ise4r', 'brandon@fullmetalworkshop.com');
		$user = new WP_User( $userID );
		$user->set_role('contributor');
		return $userID;
   	}else{
	   	$registerError;
	   	if(username_exists('brandon2')){
		   	$registerError = 'username';
	   	}
	   	if(email_exists('brandon@fullmetalworkshop.com')){
		   	$registerError = 'email';
	   	}
	   	return $registerError;
   	}
*/
   	die();
}

function submit_user_genres(WP_REST_Request $request){
	$genres = json_decode($request['genres']);
	$userid = $request['userid'];
	update_field('my_genres', $genres, 'user_'.$userid);
	return 1;
	die();
}

/*--------------------------------*\
	PROJECTS POST TYPE
\*--------------------------------*/

add_action('init', 'add_custom_posttypes');
function add_custom_posttypes() {

	$labels = array(
		'name' => _x('Stories', 'post type general name'),
		'singular_name' => _x('Story', 'post type singular name'),
		'add_new' => _x('Add New', 'Story'),
		'add_new_item' => __('Add New Story'),
		'edit_item' => __('Edit Story'),
		'new_item' => __('New Story'),
		'view_item' => __('View Story'),
		'search_items' => __('Search Story'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'show_in_rest'       => true,
		'query_var' => true,
		//'menu_icon' => get_template_directory_uri() . '/images/icons/projects.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail','page-attributes', 'author'),
		'taxonomies' => array('category', 'genre'),
	  );

	register_post_type( 'story' , $args );

	$labels = array(
		'name' => _x('Chapters', 'post type general name'),
		'singular_name' => _x('Chapter', 'post type singular name'),
		'add_new' => _x('Add New', 'Chapter'),
		'add_new_item' => __('Add New Chapter'),
		'edit_item' => __('Edit Chapter'),
		'new_item' => __('New Chapter'),
		'view_item' => __('View Chapter'),
		'search_items' => __('Search Chapter'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_template_directory_uri() . '/images/icons/projects.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => null,
		'supports' => array('title','editor','page-attributes', 'author'),
		'taxonomies' => array('category', 'genre'),
	  );

	register_post_type( 'chapter' , $args );

	$labels = array(
		'name' => _x('Locations', 'post type general name'),
		'singular_name' => _x('Location', 'post type singular name'),
		'add_new' => _x('Add New', 'Location'),
		'add_new_item' => __('Add New Location'),
		'edit_item' => __('Edit Location'),
		'new_item' => __('New Location'),
		'view_item' => __('View Location'),
		'search_items' => __('Search Location'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_template_directory_uri() . '/images/icons/projects.png',
		'rewrite' => true,
		'show_in_rest'       => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => null,
		'supports' => array('title', 'author'),
		'taxonomies' => array('category', 'genre'),
	  );

	register_post_type( 'story_location' , $args );

	$labels = array(
		'name' => _x('Daily Stories', 'post type general name'),
		'singular_name' => _x('Daily Story', 'post type singular name'),
		'add_new' => _x('Add New', 'Daily Story'),
		'add_new_item' => __('Add New Daily Story'),
		'edit_item' => __('Edit Daily Story'),
		'new_item' => __('New Daily Story'),
		'view_item' => __('View Daily Story'),
		'search_items' => __('Search Daily Story'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_template_directory_uri() . '/images/icons/projects.png',
		'rewrite' => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => null,
		'supports' => array('title','editor','thumbnail', 'page-attributes', 'author'),
		'taxonomies' => array('category', 'genre'),
	  );

	register_post_type( 'daily' , $args );

	$labels = array(
		'name' => _x('Prompts', 'post type general name'),
		'singular_name' => _x('Prompt', 'post type singular name'),
		'add_new' => _x('Add New', 'Prompt'),
		'add_new_item' => __('Add New Prompt'),
		'edit_item' => __('Edit Prompt'),
		'new_item' => __('New Prompt'),
		'view_item' => __('View Prompt'),
		'search_items' => __('Search Prompts'),
		'not_found' =>  __('Nothing found'),
		'not_found_in_trash' => __('Nothing found in Trash'),
		'parent_item_colon' => ''
	);

	$args = array(
		'labels' => $labels,
		'public' => true,
		'publicly_queryable' => true,
		'show_ui' => true,
		'query_var' => true,
		//'menu_icon' => get_template_directory_uri() . '/images/icons/projects.png',
		'rewrite' => true,
		'show_in_rest'       => true,
		'capability_type' => 'post',
		'hierarchical' => true,
		'menu_position' => null,
		'supports' => array('title'),
		'taxonomies' => array('genre'),
	  );

	register_post_type( 'prompt' , $args );
}

/*--------------------------------*\
	CUSTOM TAXONOMIES
\*--------------------------------*/

add_action( 'init', 'custom_taxonomy_Items' );

// Register Custom Taxonomy
function custom_taxonomy_Items()  {

$labels = array(
    'name'                       => 'Genre',
    'singular_name'              => 'Genre',
    'menu_name'                  => 'Genres',
    'all_items'                  => 'All Genres',
    'parent_item'                => 'Parent Genre',
    'parent_item_colon'          => 'Parent Genre:',
    'new_item_name'              => 'New Genre',
    'add_new_item'               => 'Add New Genre',
    'edit_item'                  => 'Edit Genre',
    'update_item'                => 'Update Genre',
    'separate_items_with_commas' => 'Separate Genres with commas',
    'search_items'               => 'Search Genres',
    'add_or_remove_items'        => 'Add or remove Genres',
    'choose_from_most_used'      => 'Choose from the most used Genres',
);
$args = array(
    'labels'                     => $labels,
    'hierarchical'               => true,
    'public'                     => true,
    'show_ui'                    => true,
    'show_admin_column'          => true,
    'show_in_nav_menus'          => true,
    'show_in_rest'				 => true,
    'show_tagcloud'              => true,
);
register_taxonomy( 'genre', array('story', 'chapter', 'story_location', 'daily'), $args );
register_taxonomy_for_object_type( 'genre', array('story', 'chapter', 'story_location', 'daily') );
}

function my_acf_google_map_api( $api ){
	$api['key'] = 'AIzaSyAgLo32f3ny4zExHNT8GJizx_tyY19epXM';
	return $api;
}

add_filter('acf/fields/google_map/api', 'my_acf_google_map_api');

function get_nearby_locations(WP_REST_Request $request){
	$nearLocs = get_nearby_stories($request['lat'], $request['lng'], $request['dist'], $request['id']);
	$storyArray = array();
	$locObjs = ['relation' => 'OR'];
	foreach($nearLocs as $nLoc){
		$latlng = array($nLoc->storyLat, $nLoc->storyLng);
		$cStoryArgs = [
			'post_type' => 'story',
			'posts_per_page' => -1,
			'meta_query' => [
				'relation' => 'OR',
				[
					'key' => 'stories_x_locations',
					'compare' => 'LIKE',
					'value' => $nLoc->post_id
				]
			]
		];
		$connectedstory = new WP_Query($cStoryArgs);
		if ( $connectedstory->have_posts() ) :
			while ( $connectedstory->have_posts() ) : $connectedstory->the_post();
				$location = get_field('stories_x_locations');
				$genres = get_story_genres(get_the_id());
				$authorId = get_the_author_id();
				$storyObj = (object)array();
				$storyObj->title = get_the_title();
				$storyObj->id = $nLoc->post_id;
				$storyObj->loc = $latlng;
				$storyObj->genres = $genres;
				$storyImage = wp_get_attachment_image_url(get_post_thumbnail_id(), 'medium');
				if($storyImage == null){
					$storyImage = $genres[0]->image;
				}
				$storyObj->image = $storyImage;
				$storyObj->author = get_story_author($authorId);
					array_push($storyArray, $storyObj);
			endwhile;
			wp_reset_postdata();
		endif;
	}
	return json_encode($storyArray);
	die();
}

function get_nearby_stories($lat, $long, $distance, $author){
    global $wpdb;
    $nearbyStories = $wpdb->get_results(
    "SELECT DISTINCT
        story_latitude.post_id,
        story_latitude.meta_value as storyLat,
        story_longitude.meta_value as storyLng,
        ((ACOS(SIN($lat * PI() / 180) * SIN(story_latitude.meta_value * PI() / 180) + COS($lat * PI() / 180) * COS(story_latitude.meta_value * PI() / 180) * COS(($long - story_longitude.meta_value) * PI() / 180)) * 180 / PI()) * 60 * 1.1515) AS distance
    FROM
        wp_postmeta AS story_latitude
        LEFT JOIN wp_postmeta as story_longitude ON story_latitude.post_id = story_longitude.post_id
        INNER JOIN wp_posts ON wp_posts.ID = story_latitude.post_id
    WHERE story_latitude.meta_key = 'story_lat' AND story_longitude.meta_key = 'story_lng' AND wp_posts.post_author != $author
    HAVING distance < $distance
    ORDER BY distance ASC;"
    );

    if($nearbyStories){
        return $nearbyStories;
    }
}

function save_user_image(WP_REST_Request $request){
	$attach_id = save_image($request['data'], $request['username'], $request['userid']);
	update_field('user_photo', $attach_id, 'user_'.$request['userid']);
	return wp_get_attachment_image_url($attach_id, 'small');
}

function save_image($imgData, $username, $userid) {

  if (isset($imgData)) {

    $title = $username;


	$upload_dir       = wp_upload_dir();

	// @new
	$upload_path      = str_replace( '/', DIRECTORY_SEPARATOR, $upload_dir['path'] ) . DIRECTORY_SEPARATOR;

	$img = $imgData;
	$img = str_replace('data:image/png;base64,', '', $img);
	$img = str_replace(' ', '+', $img);

	$decoded          = base64_decode($img) ;

	$filename         = 'user_'.$userid.'.png';

	$hashed_filename  = md5( $filename . microtime() ) . '_' . $filename;

	// @new
	$image_upload     = file_put_contents( $upload_path . $hashed_filename, $decoded );

	//HANDLE UPLOADED FILE
	if( !function_exists( 'wp_handle_sideload' ) ) {

	  require_once( ABSPATH . 'wp-admin/includes/file.php' );

	}

	// Without that I'm getting a debug error!?
	if( !function_exists( 'wp_get_current_user' ) ) {

	  require_once( ABSPATH . 'wp-includes/pluggable.php' );

	}

	// @new
	$file             = array();
	$file['error']    = '';
	$file['tmp_name'] = $upload_path . $hashed_filename;
	$file['name']     = $hashed_filename;
	$file['type']     = 'image/png';
	$file['size']     = filesize( $upload_path . $hashed_filename );

	// upload file to server
	// @new use $file instead of $image_upload
	$file_return      = wp_handle_sideload( $file, array( 'test_form' => false ) );

	$filename = $file_return['file'];
	$attachment = array(
	 'post_mime_type' => $file_return['type'],
	 'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
	 'post_content' => '',
	 'post_status' => 'inherit',
	 'guid' => $upload_dir['url'] . '/' . basename($filename)
	 );
	$attach_id = wp_insert_attachment( $attachment, $filename);
	require_once(ABSPATH . 'wp-admin/includes/image.php');
	$attach_data = wp_generate_attachment_metadata( $attach_id, $filename );
	wp_update_attachment_metadata( $attach_id, $attach_data );
	$jsonReturn = array(
	  'Status'  =>  'Success'
	);

	    // Insert the post into the database
	return $attach_id;

  }

}

/*--------------------------------*\
	TWITTER CRON JOB
\*--------------------------------*/

//add_action( 'init', 'schedule_tweets');

function schedule_tweets(){
	wp_schedule_event( time(), 'hourly', 'get_tweets' );
}


add_action( 'get_tweets',  'get_tweets_daily' );

function get_tweets_daily(){
	if (!class_exists('TwitterOAuth')) {
	  require_once('oauth/twitteroauth.php');
	} else {
	  define('TFD_USING_EXISTING_LIBRARY_TWITTEROAUTH',true);
	}
    $access_token = "22973792-6xTYxHIDYKGestxvIuVA8tbChUiaR20mOJqZXI0nV";
	$access_token_secret = "Fi7EY1DY3SBBw4AH98tFyPNQsb8Zy8grkxzHZbMB0do5F";
	$connection = new TwitterOAuth('LdFKGidGsSvnKyoXjTMDA', 'Th6TIcNQFZ8tSOLdVuJr4HMFddvjNDTA7avYQ3DSE', $access_token, $access_token_secret);
	$tweets = $connection->get("statuses/user_timeline", ["count" => 1, "exclude_replies" => true, "screen_name" => "DailyPrompt"]);
	$tweet = $tweets[0];

	if(is_array($tweet)){
		$tweetTitle = $tweet['text'];
		if(get_page_by_title( $tweetTitle, 'OBJECT', 'prompt' ) === NULL){
			$tweetArgs = array('post_title' => $tweet['text'], 'post_type' => 'prompt', 'post_status' => 'publish');
			$tweetID = wp_insert_post($tweetArgs);
			if(isset($tweetID) && $tweetID != 0){
				echo $tweetID;
				die();
			}
		}else{
			echo 'post exists';
			die();
		}
	}
}

function get_daily($data){
	$args = array('post_type' => 'prompt', 'posts_per_page' => 1);
	$prompt_query = new WP_Query( $args );
	if ( $prompt_query->have_posts() ) :
		$dailyObj = (object)array();
		$promptObj = (object)array();
		while ( $prompt_query->have_posts() ) : $prompt_query->the_post();
			$promptId = get_the_id();
			$prompt = get_the_title();
			$promptObj->id = $promptId;
			$promptObj->text = $prompt;
			$connectedchapter = new WP_Query( [
				'posts_per_page' => 1,
				'post_author' => $data['user'],
				'post_type' => 'daily',
				'meta_query' => [
					[
						'key' => 'prompts_x_dailies',
						'compare' => 'LIKE',
						'value' => $promptId
					]
				],
				'post_status' => 'any'
			] );
			if ( $connectedchapter->have_posts() ) : // get the stories associated with the post
				$chapObj = (object)array();
				while ( $connectedchapter->have_posts() ) : $connectedchapter->the_post();
					$chapObj->title = get_the_title();
					$chapObj->id = get_the_id();
					$genres = get_story_genres(get_the_id());
					$dailyImage = wp_get_attachment_image_url(get_post_thumbnail_id(), 'medium');
					if($dailyImage == null){
						$dailyImage = $genres[0]->image;
					}
					$chapObj->text = apply_filters( 'the_content', get_the_content());
					$chapObj->genres = $genres;
					$chapObj->type = 'daily';
					$chapObj->image = $dailyImage;
					$chapObj->status = get_post_status();
				endwhile;
				wp_reset_postdata();
				$dailyObj->chapter = $chapObj;
			endif;
		endwhile;
		wp_reset_postdata();
		$dailyObj->prompt = $promptObj;
		return json_encode($dailyObj);
		die();
	endif;
}

function get_todays_stories($data) {
	$promptID = $data['prompt'];
	$dailyArgs = array(
		'post_type' => 'daily',
		'posts_per_page' => -1,
		'meta_query' => [
			[
				'key' => 'prompts_x_dailies',
				'compare' => 'LIKE',
				'value' => $promptID
			]
		],
	);
	$daily_query = new WP_Query( $dailyArgs );
	if ( $daily_query->have_posts() ) :
		$dailyStories = array();
		while ( $daily_query->have_posts() ) : $daily_query->the_post();
			$storyObj = (object)array();
			$storyId = get_the_id();
			$storyObj->title = get_the_title();
			$storyObj->id = $storyId;

			$authorId = get_post_field( 'post_author', $storyId );
			$storyObj->author = get_story_author($authorId);

			$genres = get_story_genres($storyId);
			$storyImage = wp_get_attachment_image_url(get_post_thumbnail_id(), 'medium');
			if($storyImage == null){
				$storyImage = $genres[0]->image;
			}
			$storyObj->genres = $genres;
			$storyObj->type = 'daily';
			$storyObj->image = $storyImage;
			$storyObj->dailytext = get_the_content();
			if($dStories = get_field('prompts_x_dailies')) {
				foreach($dStories as $dStory) {
					$prompt = get_the_title($dStory);
					$date = get_the_date('F j, Y', $dStory);
					$storyObj->prompt = array('prompt_text'=>$prompt, 'date' => $date);
				}
			}
			array_push($dailyStories, $storyObj);
		endwhile;
		return json_encode($dailyStories);
		wp_reset_postdata();
	endif;
}
