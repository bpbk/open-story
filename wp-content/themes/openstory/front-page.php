<?php get_header(); ?>
<?php if(has_post_thumbnail(get_option('page_on_front'))){ ?>
	<div class="view_background" style="background-image:url(<?php echo wp_get_attachment_image_url( get_post_thumbnail_id(), 'medium' ); ?>)">
	</div>
<?php } ?>
<section id="content" role="main">

<?php if (is_user_logged_in()) { ?>
    	<a href="<?php echo wp_logout_url( home_url() ); ?>">Logout</a>
<?php } else { get_template_part('ajax', 'auth'); ?>            	
        <a class="login_button" id="show_login" href="">Login</a>
        <a class="login_button" id="show_signup" href="">Signup</a>
<?php } ?>

<?php $allGenres = get_terms(array('hide_empty' => false, 'taxonomy' => 'genre')); ?>
<?php foreach($allGenres as $genre){ ?>
	<?php print $genre->name; ?>
<?php } ?>

<?php $userID = get_current_user_id(); ?>
<?php $sLat = ''; ?>
<?php $sLng = ''; ?>
<?php $args = array('author' => $current_user->ID, 'post_type' => 'story'); ?>
<?php $story_query = new WP_Query( $args );  ?>
<?php if ( $story_query->have_posts() ) : ?>
	<?php while ( $story_query->have_posts() ) : $story_query->the_post(); ?>
		<?php $genres = get_the_terms(get_the_id(), 'genre'); ?>
		<div class="my_story<?php foreach($genres as $genre){ echo ' genre_'.$genre->slug; }; ?>">
			<h3><?php the_title(); ?></h3>
			<?php foreach($genres as $genre){ ?>
				<p><?php echo $genre->name; ?></p>
			<?php } ?>
			<?php $locs = get_story_locations(get_the_id()); ?>
		</div>
	<?php endwhile; ?>
	<?php $sLat = $locs[0][0]; ?>
	<?php $sLng = $locs[0][1]; ?>
	<?php wp_reset_postdata(); ?>
<?php endif; ?>

<div id="my_story_map"></div>
<script>
      function initMap() {
        var uluru = {lat: <?php echo $sLat; ?>, lng: <?php echo $sLng; ?>};
        var map = new google.maps.Map(document.getElementById('my_story_map'), {
          zoom: 15,
          center: uluru,
          disableDefaultUI: true
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
</script>
<!--<script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDYPz44WcEeOZgxVY06V2S72dp4ntSeyc8&callback=initMap"> 
</script>-->

<script>
	var app = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
       // app.receivedEvent('deviceready');
       navigator.geolocation.getCurrentPosition(app.onSuccess, app.onError);
    },

    onSuccess: function(position){
        var longitude = position.coords.longitude;
        var latitude = position.coords.latitude;
        var latLong = new google.maps.LatLng(latitude, longitude);

        var mapOptions = {
            center: latLong,
            zoom: 13,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var map = new google.maps.Map(document.getElementById("my_story_map"), mapOptions);
	
        var marker = new google.maps.Marker({
              position: latLong,
              map: map,
              title: 'my location'
          });
    },
    
    onError: function(error){
        alert("the code is " + error.code + ". \n" + "message: " + error.message);
    },
};

app.initialize();
</script>
<div id="start_story_view">
	<div class="inner_content">
		<form id="chapter">
			<input id="story_title" placeholder="story title" />
			<input id="chapter_title" placeholder="chapter title" />
			<textarea id="chapter_text"></textarea>
			<input id="submit_chapter" type="submit" value="submit" />
		</form>
	</div>
</div>
</section>
<?php get_footer(); ?>