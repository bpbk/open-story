var $ = jQuery.noConflict();

$(document).ready(function(){
	clickFunctions();
});

function clickFunctions(){
	$('#submit_chapter').click(function(){
		var storyTitle = $('#story_title').val();
		var chapterTitle = $('#chapter_title').val();
		var chapterText = $('#chapter_text').val();
		createStory(storyTitle, chapterTitle, chapterText);
		return false;
	});
	
	$('#start_story_button').click(function(){
		$('#start_story_view').toggleClass('active');
		return false;
	});
}

function createStory(storyTitle, chapterTitle, chapterText){
	$.ajax({
		url: ajaxurl,
		data: {
			'action': 'do_ajax',
			'fn': 'create_story',
			'sTitle': storyTitle,
			'cTitle': chapterTitle,
			'cText':chapterText
		},
		//dataType: 'json',
		success: function(data) {
			//var nlJson = $.parseJSON( data );
			console.log(data);
		},
		complete:function(){
		},
		error: function(errorThrown) {
			console.log(errorThrown);
		}
	});
}